# install dependencies
npm install

# serve with hot reload at l
# this port is often occupied, but you can use another port with the command below
npm run dev
# run with your own local domain & port (e.g. project-layout.local:5050)
# add your local domain to etc/hosts (e.g. project-layout.local)
# start with your desired port (e.g. 5050)
npm run dev -- --open --port 5050

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
